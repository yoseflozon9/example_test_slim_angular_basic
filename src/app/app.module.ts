import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
 import {Test1Service } from './test1.service';
import { UserFromsComponent } from './user-froms/user-froms.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UserFromsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
       FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot([
      {path: '', component: ProductsComponent},
      {path: 'users', component: UsersComponent},
      {path: '**', component:NotFoundComponent}
    
         ])
      ],
  
  providers: [Test1Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
