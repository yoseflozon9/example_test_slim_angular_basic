import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFromsComponent } from './user-froms.component';

describe('UserFromsComponent', () => {
  let component: UserFromsComponent;
  let fixture: ComponentFixture<UserFromsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFromsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFromsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
