import { Component, OnInit } from '@angular/core';
import {Test1Service } from '../test1.service';
@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 usersKeys = [];
 users;
 constructor(private service:Test1Service) {
  
        // let service = new MessagesService();
         service.getUser().subscribe(
           response=>{
            // console.log(response.json());
             this.users = response.json();
            this.usersKeys = Object.keys(this.users);
         }); //הרשמה ל observable
   }

optimisticAdd(users){
     console.log("addMessage work "+users); 
      var newKey = parseInt(this.usersKeys[this.usersKeys.length - 1],0) + 1;
      var newUserObject = {};
     newUserObject['body'] = users; //גוף ההודעה עצמה
      this.users[newKey] = newUserObject;
      this.usersKeys = Object.keys(this.users);

 }

  //תזמון
    pessimisticAdd(){
      this.service.getUser().subscribe(
       response=>{
          this.users = response.json();
         this.usersKeys = Object.keys(this.users);
    });
   }

  //מחיקת רשומה
   deleteUsers(key){
      console.log.apply(key);
     let index = this.usersKeys.indexOf(key);
     this.usersKeys.splice(index,1); //אחד מבטא מחיקת רשומה אחת
 

      //delete from server
     this.service.deleteUsers(key).subscribe(
       response=>console.log(response)
     );

    }
  ngOnInit() {
  }

}
